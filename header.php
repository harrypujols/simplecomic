<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<!--meta tags-->
<meta name="description" content="<?php bloginfo('description'); ?>" />
<meta name="keywords" content="web, site, beautiful, end, world, comic, comics, webcomics, comix, cartoon," />
<title><?php bloginfo('name'); ?></title>
<!-- feed -->
<link href="<?php bloginfo('rss2_url'); ?>" title="RSS - <?php bloginfo('name'); ?>" type="application/rss+xml" rel="alternate">
<link href="<?php bloginfo('atom_url'); ?>" title="Atom - <?php bloginfo('name'); ?>" type="application/atom+xml" rel="alternate">
<!--mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<!--style tags-->
<link href="<?php bloginfo('stylesheet_directory'); ?>/_/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/_/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" media="all" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/_/bootstrap/css/font-awesome.css" rel="stylesheet" media="all" />
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" />
<!-- icons -->
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/_/img/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="<?php bloginfo('stylesheet_directory'); ?>/_/img/icon-57.png" sizes="57x57"/>
<link rel="apple-touch-icon" href="<?php bloginfo('stylesheet_directory'); ?>/_/img/icon-114.png" sizes="114x114"/>
<!-- le wordpress head tags -->
<?php wp_head(); ?>
 <!--[if IE]>
  <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/_/js/modernizr.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/_/js/ie.js"></script>
  <link href="<?php bloginfo('stylesheet_directory'); ?>/_/bootstrap/css/font-awesome-ie7.css" rel="stylesheet" media="all" />
  <link href="<?php bloginfo('stylesheet_directory'); ?>/_/css/ie.css" rel="stylesheet" media="all" />
  <![endif]--> 
</head>
<body <?php body_class(); ?>>
 <div id="container" <?php post_class(); ?>>
 <header class="navbar head-bar">
        <nav class="container">
            <ul class="nav">
           	  <li><a href="<?php bloginfo('url'); ?>" title="Home"><i class="icon-home icon-large"></i></a></li>
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Share"><i class="icon-info-sign icon-large"></i></a>
              	<ul class="dropdown-menu">
	              	<?php dynamic_sidebar('info'); ?>
	            </ul>
	          </li>
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-share icon-large"></i></a>
              	<ul class="dropdown-menu">
	              <li><a href="http://twitter.com/home?status=<?php the_title(); ?>%20-%20<?php bloginfo('name'); ?>%20<?php the_permalink() ?>" title="Share on Twitter">Twitter</a></li>
	              <li><a href="http://www.facebook.com/share.php?u=<?php the_permalink() ?>" title="Share on Facebook">Facebook</a></li>
	              <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share on Google+">Google+</a></li>
	            </ul>
	          </li>
            </ul><!-- nav -->
        </nav><!-- container -->
    </header><!-- navbar -->
    
    <header class="navbar sub-bar">
        <nav class="container">
            <ul class="nav pull-right">
           	  <li>
           	   <?php  
	           	   $bookmark = get_bookmark(1);
	           	   echo '<a href="'.$bookmark->link_url.'" title="Twitter"><i class="icon-twitter icon-large"></i></a>';
	           ?>
           	  </li>
              <li>
               <?php  
	           	   $bookmark = get_bookmark(2);
	           	   echo '<a href="'.$bookmark->link_url.'" title="Facebook"><i class="icon-facebook-sign icon-large"></i></a>';
	           ?>
              </li>
              <li><a href="<?php bloginfo('rss2_url'); ?>" title="RSS"><i class="icon-rss icon-large"></i></a></li>
            </ul><!-- nav -->
        </nav><!-- container -->
    </header><!-- navbar -->