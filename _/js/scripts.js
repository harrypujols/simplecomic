/* ===================================================
 * Simple Comics theme jquery hacks
 * ===================================================
 * Prepared by Harry Pujols himself
 * ========================================================== */
 
!function ($) {

  $(function () {
  
  	 /* dropdown menu hack */
  	$('.dropdown-menu .widgettitle').remove();
  	 /* hack for custom widget title */
  	$('.span4 .widgettitle').addClass('page-header');
     /* search form hack */
     $('#searchform').addClass('form-search');
	 $('#s').addClass('search-query');
	 $('#searchsubmit').addClass('btn').addClass('btn-danger');
	 /* comments hacks */
	 $('#submit').addClass('btn').addClass('btn-primary');
	 $('#comments').addClass('label').addClass('label-important');
	 $('.commentlist').addClass('nav').addClass('nav-stacked');
	 $('comment-author').addClass('breadcrumb');
	 $('.comment-edit-link').empty().append('<i class="icon-pencil"></i>');
	 $('.reply a').addClass('btn').addClass('btn-warning').addClass('btn-mini').empty().append('<i class="icon-comment"></i> Reply');
	 $('#cancel-comment-reply a').addClass('btn').addClass('btn-warning').addClass('btn-mini').empty().append('<i class="icon-ban-circle"></i> Cancel Reply');
	 $('a[title="Log out of this account"]').addClass('btn').addClass('btn-danger').addClass('btn-mini').empty().append('<i class="icon-signout"></i> Logout');
	 /* clone facebook comments code to where I want my comments to reside */
	 $('.fb-comments').clone().appendTo('#discussion');
	 $('article .fb-comments').hide();
	 /* to style the calendar widget */
	 $('#wp-calendar').addClass('table').addClass('table-condensed');
	 /* to style the characters page */
	 $('#characters p:not(:has(img))').addClass('caption');
	 /* in the characters page removes the image that is not the thumbnail post */
	 $('.thumbnail img:not(.wp-post-image)').remove();
	 /* styles the non-comic pages */
	 $('#post').addClass('span4 offset4');
	 $('#post').parent().addClass('container');
  })

}(window.jQuery);