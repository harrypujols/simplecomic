   <footer id="footer">
	 <hr>
		<small>Copyright &copy; <?php echo get_the_time(Y); ?> <?php wp_list_authors('html=0'); ?>. All rights reserved.</small>
	</footer>
</div><!-- container -->
<?php wp_footer(); ?>
<!--additional scripts tags-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/_/bootstrap/js/bootstrap.js"></script>
</body>
</html>