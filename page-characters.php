<?php 
/*
Template Name: Characters
*/	
?>
<?php get_header(); ?>
    <div class="container">
<h1 class="page-header">
<?php echo $post->post_title; ?>
</h1>
<div class="row">
<ul class="thumbnails">
	<?php query_posts( array ( 'category_name' => 'characters', 'posts_per_page' => -1, 'order' => 'ASC' ) ); ?>
  	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <li class="span3">
    <div class="thumbnail">
     <p><?php if ( has_post_thumbnail()) the_post_thumbnail('small-thumb'); ?></p>
      <h5><?php the_title(); ?></h5>
      <?php the_content(); ?>
    </div>
  </li>
  	<?php endwhile; else: ?>
    	<div class="alert alert-block">
	    	<h4 class="alert-heading"><i class="icon-warning-sign"></i> Under Construction</h4>
	    	<p>We promise you there will be something here shortly. Check back soon.</p>
	    	<p><a class="btn btn-warning" href="<?php bloginfo('url'); ?>">Okay</a></p>
	    </div>
    <?php endif; ?>
</ul>
</div>

<?php get_footer(); ?>