<!--  list of all posts -->
 <aside class="container">
<nav id="nav-buttons">
  	<ul>
 		<li><?php previous_post_link('%link', '<i class="icon-caret-left icon-large"></i>', TRUE); ?></li><!-- prev -->
 		<li><?php next_post_link('%link', '<i class="icon-caret-right icon-large"></i>', TRUE); ?></li><!-- next -->
  	</ul>
 </nav>
 <nav class="pagination pagination-centered">
  	<ol>
    <?php
        $lastposts = get_posts('order=ASC&posts_per_page=-1&category_name=comic');
        foreach($lastposts as $post) :
        setup_postdata($post); ?>
        <li<?php if ( $post->ID == $wp_query->post->ID ) { echo ' class="active"'; } else {} ?>>
        <a href="<?php the_permalink() ?>">
        <!-- lists the page numbers -->
	        <?php $page++; 
		          echo $page;
		    ?>
		</a></li>
    <?php endforeach; ?>
    </ol>
  </nav>
</aside>
<section id="discussion" class="well">
<?php comments_template(); ?> 
</section>