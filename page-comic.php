<?php get_header(); ?>

 <section id="main-content">
 <!-- start content -->
 <?php query_posts( array ( 'category_name' => 'comic', 'posts_per_page' => 1 ) ); ?>
  	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
       <article <?php post_class(); ?> id="comic">
<!-- post content -->
    	<?php the_content(); ?>
    	    </article>
    	<?php endwhile; else: ?>
    	<div class="container alert alert-error">
    	 	<p>This page is empty. Looking for something else?</p>
    	 	<p class="form-search"><?php get_search_form(); ?></p>
    	</div><!-- alert -->
    	<?php endif; ?>
<!-- end of loop --> 
 </section>
  
<?php get_sidebar(); ?>
<?php get_footer(); ?>