<?php

//register jquery and custom scripts
wp_enqueue_script('scripts', get_bloginfo('stylesheet_directory').'/_/js/scripts.js', array('jquery'), time());

//register sidebar widgets
if ( function_exists('register_sidebar') )
register_sidebar(array(
	'name'=>'info',
));
register_sidebar(array(
	'name'=>'custom',
));
register_sidebar(array(
	'name'=>'social',
));

// post thumbnail support
	if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );
	if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'large-thumb', 360, 268 );
	add_image_size( 'small-thumb', 260, 180, true );
}

//custom header image support
$defaults = array(
	'default-image'          => get_template_directory_uri() . '/_/img/masthead.png',
	'random-default'         => true,
	'width'                  => 1170,
	'height'                 => 315,
	'flex-height'            => true,
	'flex-width'             => true,
	'default-text-color'     => '',
	'header-text'            => true,
	'uploads'                => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );

?>