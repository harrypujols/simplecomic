<?php 
/*
Template Name: About
*/	
?>
<?php get_header(); ?>
    <div class="container">
    <div class="hero-image">
    <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
    </div>
        <h1 class="page-header"><?php bloginfo('name'); ?></h1>
        <p><?php bloginfo('description'); ?></p>

      <!-- Example row of columns -->
      <div class="row">
      <div class="span4">
  	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
       <h2 class="page-header"><?php the_title(); ?></h2>
    	<?php the_content(); ?>
    	       <p>
    	<?php  
	        $bookmark = get_bookmark(3);
	    	echo '<a class="btn" href="'.$bookmark->link_url.'">'.$bookmark->link_name.'</a>';
	     ?>
    	<?php endwhile; else: ?>
    	<?php endif; ?>
       </p>
      </div><!-- span4 -->
        <div class="span4">
         <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('custom')) : ?>         
        <p>
    	<?php  
	        $bookmark = get_bookmark(4);
	    	echo '<a class="btn" href="'.$bookmark->link_url.'">'.$bookmark->link_name.'</a>';
	     ?>
       </p>
         <?php endif; ?>
        </div><!-- span4 -->
        <div class="span4">
         <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('social')) : ?>         
        <p>
    	<?php  
	        $bookmark = get_bookmark(5);
	    	echo '<a class="btn" href="'.$bookmark->link_url.'">'.$bookmark->link_name.'</a>';
	     ?>
       </p>
         <?php endif; ?>
        </div><!-- span4 -->
      </div><!-- row -->
      
<?php get_footer(); ?>