<?php get_header(); ?>

 <section id="main-content">
 <!-- start content -->
  	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
       
       <!-- conditional title -->
        <?php if ( in_category('comic') ) { ?>
           <article <?php post_class(); ?> id="comic">
	    <?php } else { ?>
	    	<article <?php post_class(); ?> id="post">
           <h1 class="page-header"><?php the_title(); ?></h1>
	    <?php } ?>
       <!-- post content -->
    	<?php the_content(); ?>
      </article>
    	<?php endwhile; else: ?>
    	<p><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/_/img/fail-whale.gif" alt="Fail Whale" /></a></p>
    	<div class="container alert alert-error">
    	 	<p>This page has been seized by a Fail Whale. Looking for something else?</p>
    	 	<p class="form-search"><?php get_search_form(); ?></p>
    	</div><!-- alert -->
    	<?php endif; ?>
<!-- end of loop --> 
 </section><!-- main content -->
<!-- conditional sidebar -->
        <?php if ( in_category('comic') ) { ?>
           <?php get_sidebar(); ?>
	    <?php } ?>
<?php get_footer(); ?>