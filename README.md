simplecomic
===========

A Wordpress theme for my comic books, built on the Twitter Bootstrap framework.


How to use the theme
=====================

Works best when you set is as "one post per page". 
It is untested with several posts per page, but I imagine it's going to look ugly.


How to use the "info" widget
============================

The title of the widget section won't show up on the menu. Nested links will behave similar to main links.


How to use Twitter and Facebook links
=====================================

These links are supposed to be the first two links from the default Blogroll.
Change the current links to your Twitter and Facebook pages and you're all set.