<?php get_header(); ?>

 <section id="main-content">
 <!-- start content -->
  	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
       <article <?php post_class(); ?>>
<!-- post content -->
    	<?php the_content(); ?>
    	    </article>
    	<?php endwhile; else: ?>
    	<div class="container alert alert-error">
    	 	<p>This page is empty. Looking for something else?</p>
    	 	<p class="form-search"><?php get_search_form(); ?></p>
    	</div><!-- alert -->
    	<?php endif; ?>
<!-- end of loop --> 
 </section>

	<footer class="navbar-fixed-bottom">
	 <hr>
		<small>Copyright &copy; <?php echo get_the_time(Y); ?> <?php wp_list_authors('html=0'); ?>. All rights reserved.</small>
	</footer>
</div><!-- container -->
<?php wp_footer(); ?>
</body>
</html>